package tests;

import DAO.ArticleDAO;
import DAO.PasswordDAO;

import java.sql.Blob;

public class testPasswordWork {
    public static void main(String[] args) {

        // iterations - random 32-64; -> to database
        // ask user to enter a password, generate iterations number (rnd 32-64)- > to db,
        // generate salts
        int iter = 64;


        Utils.Passwords passwords = new Utils.Passwords();
        byte[]salt;
        System.out.print("salt: ");
        salt = passwords.getNextSalt(iter);
        for (int i = 0; i < salt.length; i++) {
            System.out.print(salt[i] + ", ");
        }

        String saltString = passwords.base64Encode(salt);

        System.out.println();

        String password = "myPassword";

        byte hashMy[];

        hashMy = passwords.hash(password.toCharArray(), salt, iter);
        System.out.print("HASH: ");
        for (int i = 0; i < hashMy.length; i++) {
            System.out.print(hashMy[i] + ", ");
        }


        System.out.println();
        String passInString = passwords.base64Encode(hashMy);
        System.out.println("pass in string: " + passInString);

//        decoding - checking the password

        System.out.println("salt in string:" + saltString);

        byte [] hashFromString = passwords.base64Decode(passInString);
        System.out.println(passwords.isExpectedPassword("myPassword".toCharArray(), passwords.base64Decode(saltString), iter, hashFromString));


        System.out.println("-------");

        String config = "web/WEB-INF/mysqlHome.properties";

        PasswordDAO passwordDAO = new PasswordDAO(config);
        byte [] b = passwordDAO.getSaltByID(3);
//        for (int i = 0; i < b.length; i++) {
//            System.out.println(b[i]);
//        }
//        System.out.println("!!");



//        passwordDAO.pushData(9, new byte[]{1,2}, 3);
//        for (int i = 0; i < b.length; i++) {
//            System.out.println(b);
//        }


//        System.out.println(passwords.isExpectedPassword("myPassword".toCharArray(), salt, iter, hashMy));


//        passwords.hash (password.toCharArray(), salt);


    }


}
