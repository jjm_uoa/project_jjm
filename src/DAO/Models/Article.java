package DAO.Models;

import java.sql.Time;



    import java.sql.Time;

    public class Article {

        int article_id;
        String author_fname;
        String author_lname;
        String title;
        String content;
        Time created_time;

        public Article(int article_id, String author_fname, String author_lname,int userID, String title, String content, Time created_time){
            this.article_id = article_id;
            this.author_fname = author_fname;
            this.author_lname = author_lname;
            this.title= title;
            this.content = content;
            this.created_time = created_time;



        }

        public int getArticle_id() {
            return article_id;
        }

        public void setArticle_id(int article_id) {
            this.article_id = article_id;
        }

        public String getAuthor_fname() {
            return author_fname;
        }

        public void setAuthor_fname(String author_fname) {
            this.author_fname = author_fname;
        }

        public String getAuthor_lname() {
            return author_lname;
        }

        public void setAuthor_lname(String author_lname) {
            this.author_lname = author_lname;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public Time getCreated_time() {
            return created_time;
        }

        public void setCreated_time(Time created_time) {
            this.created_time = created_time;
        }
    }

