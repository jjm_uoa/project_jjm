package DAO.Models;

import java.sql.Date;

public class Account {

    private Integer userId;
    private String login;
    private String firstName;
    private String lastName;
    private String middleName;
    private String password_hash;
    private String email;
    private Date birthday;
    private String country_code;
    private String country_fullName;
    private String avatarPath;
    private String avatarThumbnailPath;
    private byte[] salt;
    private Integer iterations_number;
    private String about;

    public Account() {
    }

    public Account(Integer userId, String login, String firstName, String lastName, String middleName, String email, Date birthday, String country_code, String country_fullName, String avatarPath, String avatarThumbnailPath, byte[] salt, Integer iterations_number, String about) {
        this.userId = userId;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.password_hash = password_hash;
        this.email = email;
        this.birthday = birthday;
        this.country_code = country_code;
        this.country_fullName = country_fullName;
        this.avatarPath = avatarPath;
        this.avatarThumbnailPath = avatarThumbnailPath;
        this.salt = salt;
        this.iterations_number = iterations_number;
        this.about = about;

    }

    public Integer getUserId() {
        return userId;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getPassword_hash() {
        return password_hash;
    }

    public String getEmail() {
        return email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getCountry_code() {
        return country_code;
    }

    public String getCountry_fullName() {
        return country_fullName;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public String getAvatarThumbnailPath() {
        return avatarThumbnailPath;
    }

    public byte[] getSalt() {
        return salt;
    }

    public Integer getIterations_number() {
        return iterations_number;
    }

    public String getAbout() {
        return about;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setPassword_hash(String password_hash) {
        this.password_hash = password_hash;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public void setCountry_fullName(String country_fullName) {
        this.country_fullName = country_fullName;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public void setAvatarThumbnailPath(String avatarThumbnailPath) {
        this.avatarThumbnailPath = avatarThumbnailPath;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public void setIterations_number(Integer iterations_number) {
        this.iterations_number = iterations_number;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}



