package DAO;

import DAO.Models.Article_beta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Article_betaDAO implements AutoCloseable {

    Connection connector;
    String query;
    ConnectionMySQL mySQL;
    Article_beta article_beta;


    public Article_betaDAO(String config) {
        connector = null;
        query = null;
        mySQL = new ConnectionMySQL(config);
        article_beta = new Article_beta();
    }

    // returns false - if article not added
    // method will add a new article what ever if the value with new auto incremented
    // article id unless the user_id is valid (presented in account) database
    public boolean addArticle(int user_id, String title, String content) {
        boolean returnValue = false;
        connector = mySQL.establish();
        query = "insert into article (user_id, title, content) values (?,?,?);";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, String.valueOf(user_id));
            pst.setString(2, String.valueOf(title));
            pst.setString(3, String.valueOf(content));
            int i = pst.executeUpdate();
            System.err.println(i + " row(s) affected in account");
            if (i > 0)
                returnValue = true;
        } catch (SQLException e) {
            System.err.println("Error while adding an article");
            e.printStackTrace();
        } finally {
            close();
        }
        return returnValue;
    }


    // update an article by arttilce ID
    // will update the content
    public void updateArticle(int article_id, String content) {
        connector = mySQL.establish();
        query = "update article set content = ? where article_id = ?";
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(2, String.valueOf(article_id));
            pst.setString(1, String.valueOf(content));
            System.err.println("row(s) " + pst.executeUpdate() + "affected");
        } catch (SQLException e) {
            System.err.println("Error while updating an article, article_id: " + article_id);
            e.printStackTrace();
        } finally {
            close();
        }
    }



    //get all articles in HashMap format -> will fetch all data,
    // should be used to fetch any particular data like only titles / articles / etc ...
    public List<HashMap> getAllArticles() {
        connector = mySQL.establish();
        query = "select title, content, login, created_time, modified_time  from article acc, account usr where acc.user_id =usr. id";
        ResultSet resultSet;

        HashMap<String, String> article;
        // for each article
        List<HashMap> articles_list = new ArrayList<>(); // can be one or more articles, so every article will go to separate container


        try {
            PreparedStatement pst = connector.prepareStatement(query);
            resultSet = pst.executeQuery();
            resultSet.next();

            processArticle(resultSet, articles_list);


        } catch (SQLException e) {
            System.err.println("Error while fetching data for getAllArticles module");
            e.printStackTrace();
        } finally {
            close();
        }
        return articles_list;
    }


    public List<HashMap> getAllArticles(String login) {
        System.err.println("Fetching all articles by user: " + login);
        connector = mySQL.establish();
        query = "select title, content, login, created_time, modified_time  from article acc, account usr where acc.user_id =usr. id and usr.login = ?";
        ResultSet resultSet;
        // for each article
        List<HashMap> articles_list = new ArrayList<>(); // can be one or more articles, so every article will go to separate container
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, login);
            resultSet = pst.executeQuery();
            resultSet.next();
            processArticle(resultSet, articles_list);
        } catch (SQLException e) {
            System.err.println("Error while fetching data for getAllArticles module, or no articles found.");
//            e.printStackTrace();
        } finally {
            close();
        }
        return articles_list;
    }



//util private class for getAllArticles
    private void processArticle(ResultSet resultSet, List<HashMap> articles_list) throws SQLException {
        HashMap<String, String> article;
        do {

            article = new HashMap<>(); // new object for each cycle

            article.put("title", resultSet.getString(1));
            article.put("content", resultSet.getString(2));
            article.put("login", resultSet.getString(3));
            article.put("created_time", resultSet.getString(4));
            article.put("modified_time", resultSet.getString(5));
            articles_list.add(article);
        }
        while (resultSet.next());
    }


    @Override
    public void close() {
        mySQL.close();
    }
}

