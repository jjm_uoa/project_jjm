package DAO;


import DAO.Models.Password;

import java.sql.*;

public class PasswordDAO implements AutoCloseable {

    Connection connector;
    String query;
    ConnectionMySQL mySQL;
    Password password;
    ResultSet resultSet;



    public PasswordDAO() {

        connector = null;
        query = null;
        mySQL = new ConnectionMySQL("web/WEB-INF/mysqlHome.properties");
        password = new Password();

    }

    public PasswordDAO(String config) {

        connector = null;
        query = null;
        mySQL = new ConnectionMySQL(config);
        password = new Password();

    }

    public byte[] getSaltByID(int id) {

        connector = mySQL.establish();

        query = "select salt from password_specs where id = ?";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setInt(1, id);
//            pst.execute();
            ResultSet set = pst.executeQuery();

            set.next();
            Blob blob = set.getBlob(1);
            return blob.getBytes(1, (int) blob.length());
        } catch (SQLException e) {
//            e.printStackTrace();
            System.err.println("id# " + id + " doesn't exist in database.");
        } finally {
            close();
        }
        return null;
    }

    public int getIterationsByID(int id){

        ResultSet rs;
        connector = mySQL.establish();
        int iterations = 0;

        query = "select iterations from password_specs where id = ?";


        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            rs.next();
            iterations = rs.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return iterations;
    }

    public byte[] getsaltByID (int id){


        ResultSet rs;
        connector = mySQL.establish();
        byte [] salt;

        query = "select salt from password_specs where id = ?";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            rs.next();
            salt = rs.getBytes(1);
            close();
            return salt;
        } catch (SQLException e) {
            e.printStackTrace();
            close();
        } finally {
            close();
        }

        return null;
    }

    public int pushData(int id, byte[] salt, int iter) {
        connector = mySQL.establish();

        query = "insert into password_specs (id, salt, iterations) VALUES (?,?,?)";

        int rows = 0;

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setInt(1, id);
            pst.setBytes(2, salt);
            pst.setInt(3, iter);
            rows = pst.executeUpdate();
            utilUpdate(rows);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }

        return rows;
    }




    @Override
    public void close() {
        mySQL.close();
    }

    public void utilUpdate(int i) {
        System.err.println(i + " row(s) affected in password_specs.");
    }
}
