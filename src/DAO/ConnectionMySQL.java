package DAO;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionMySQL {
//    Connection connection = null;
    Properties dbProps;
    String config;
    Connection connection;

    public ConnectionMySQL(String config) {
        dbProps = null;
        this.config = config;
        connection = null;
    }

    public Connection establish() {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        dbProps = new Properties();

        try(FileInputStream fIn = new FileInputStream(config)) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try  {
//            Class.forName("com.mysql.jdbc.Driver");
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(dbProps.getProperty("url"), dbProps);
            System.err.println("Connection to database " + dbProps.getProperty("url") + " established.");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void close (){
        try {
            if (connection != null) {
                connection.close();
                System.err.println("Connection closed...");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}