package DAO;

import DAO.Models.Account;

import java.sql.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AccountDAO implements AutoCloseable {

    Connection connector;
    String query;
    ConnectionMySQL mySQL;

    Account account;


//    public AccountDAO() {
//
//        connector = null;
//        query = null;
//        mySQL = new ConnectionMySQL("/WEB-INF/mysqlHome.properties");
//        account = new Account();
//
//    }


    public AccountDAO(String config) {

        connector = null;
        query = null;
        mySQL = new ConnectionMySQL(config);
        account = new Account();

    }

    public int pushData(Map<String, String> data) {

        connector = mySQL.establish();

        query = "insert into account (login, firstname, lastname, middlename, password_hash, email, birthday, country, avatar_path, about) VALUES (?,?,?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
//            pst.setInt(1, Integer.valueOf(data.get("id")));
            pst.setString(1, data.get("username"));
            pst.setString(2, data.get("firstname"));
            pst.setString(3, data.get("lastname"));
            pst.setString(4, data.get("middlename"));
            pst.setString(5, data.get("password_hash"));
            pst.setString(6, data.get("email"));
            pst.setString(7, data.get("birthday"));
            pst.setString(8, data.get("country"));
            pst.setString(9, data.get("file"));
            pst.setString(10, data.get("description"));

            System.err.println(pst.executeUpdate() + " row(s) affected in account");

        } catch (SQLException e) {
            e.printStackTrace();
            close();
        }
        close();

        //need to return user id

        return IDbyLogin(data.get("username"));
    }

    public int IDbyLogin(String login) {
        connector = mySQL.establish();
        query = "select id from account where login = ?";
        int id = 0;
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, login);
            ResultSet rs = pst.executeQuery();
            rs.next();
            id = rs.getInt(1);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
            close();
        }
        return id;
    }

    // UTIL - check if account in database
    public boolean isAccountExist(String account) {
        connector = mySQL.establish();
        query = "select login from account where login = ?";
        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, account);
            ResultSet rs = pst.executeQuery();
            rs.next();
            String login = rs.getString(1);
            if (login.equals("")) return false;
            else return true;
        } catch (SQLException e) {
            System.err.println("No account " + account + " found in the database.");
//            e.printStackTrace();
        } finally {
            close();
        }
        return false;
    }

    public String getHashedPassword(String login) {
        connector = mySQL.establish();
        query = "select password_hash from account where login = ?";
        String hashedPassword = "";

        try {
            PreparedStatement pst = connector.prepareStatement(query);
            pst.setString(1, login);
            ResultSet rs = pst.executeQuery();
            rs.next();
            hashedPassword = rs.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return hashedPassword;
    }

    @Override
    public void close() {
        mySQL.close();
    }
}
