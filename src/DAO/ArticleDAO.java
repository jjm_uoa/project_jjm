package DAO;

import java.sql.Connection;
import java.util.*;

import DAO.Models.Account;
import DAO.Models.Article;

import java.sql.*;
import java.util.Date;
import java.util.Map;

public class ArticleDAO implements AutoCloseable {

    Connection connector;
    ConnectionMySQL mySQL;
    String query;

    public ArticleDAO(String config){
        connector = null;
        mySQL = new ConnectionMySQL(config);
        query = null;

    }





    public List<Article> getInfofromDataBase() {
        List<Article> articles= new ArrayList<>();
        connector = mySQL.establish();
        query = "select * FROM finalProjectArticle";
        try {
        PreparedStatement pst = connector.prepareStatement(query);

        try (ResultSet r = pst.executeQuery()) {

            while (r.next()) {

                int articalid = r.getInt(1);
                String authFname=r.getString(2);
                int userid = r.getInt(3);
                String authLname = r.getString(4);
                String title = r.getString(5);
                String content = r.getString(6);
                Time time = r.getTime(7);


                Article toAdd=new Article(articalid,authFname,authLname,userid,title,content,time);
                articles.add(toAdd);


            }

        }



    } catch (SQLException e) {
        e.printStackTrace();


    }
        System.out.println(articles);
        return articles;



}

    public void addToDataBase(Map<String, String> data) {
        connector = mySQL.establish();
        query = "insert into finalProjectArticle (article_id, author_fname, user_id, author_lname, title, content, created_time) VALUES (?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = connector.prepareStatement(query);
//            pst.setInt(1, Integer.valueOf(data.get("id")));
            pst.setString(1, data.get("artID"));
            pst.setString(2, data.get("fname"));
            pst.setString(3, data.get("lname"));
            pst.setString(4, data.get("userID"));
            pst.setString(5, data.get("title"));
            pst.setString(6, data.get("content"));
            pst.setString(7, data.get("time"));





        } catch (SQLException e) {
            e.printStackTrace();

        }



    }




    @Override
    public void close() throws Exception {
        mySQL.close();

    }
}
