package servlets;

import DAO.AccountDAO;
import DAO.PasswordDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static Utils.Passwords.*;

@WebServlet(name = "Login")
public class Login extends HttpServlet {

    AccountDAO accountDAO;
    PasswordDAO passwordDAO;


    private String login;
    private String password;
    private String hashedPassword;
    private byte[] salt;
    private int iterations;

    PrintWriter printWriter;
    String logMessage;

    @Override
    public void init() throws ServletException {
        super.init();

        String path = getServletContext().getInitParameter("sql_config");

//        accountDAO = new AccountDAO(this.getServletContext().getRealPath("WEB-INF/mysqlHome.properties"));
//        passwordDAO = new PasswordDAO(this.getServletContext().getRealPath("WEB-INF/mysqlHome.properties"));

        accountDAO = new AccountDAO(this.getServletContext().getRealPath(path));
        passwordDAO = new PasswordDAO(this.getServletContext().getRealPath(path));


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        printWriter = response.getWriter();

        System.err.println("Login servlet");

        login = request.getParameter("username");
        password = request.getParameter("password");
        System.out.println("Entered password: " + login);
        System.out.println("Entered password: " + password);

        //check first if account is in database
        if (!accountDAO.isAccountExist(login)) {
            printWriter.println("Account " + login + " doesn't exist");
        } else {
            //get hashed password
            hashedPassword = accountDAO.getHashedPassword(login);
            System.err.println("hash: " + hashedPassword);

            //get user id
            int id = accountDAO.IDbyLogin(login);
            System.err.println("id:" + id);

            byte[] hashFromString = base64Decode(hashedPassword);
            int iterations = passwordDAO.getIterationsByID(id);
            System.err.println("iterations:" + iterations);
            byte[] salt = passwordDAO.getSaltByID(id);
            System.err.println("salt size=" + salt.length);
            boolean check = isExpectedPassword(password.toCharArray(), salt, iterations, hashFromString);


            System.err.println("password is expected: " + check);
            if (check)
                printWriter.println("Password is correct!");
            else
                printWriter.println("Password is WRONG!");
        }


    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
