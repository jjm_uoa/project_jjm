<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main_styling.css" type="text/css">
    <title>Sign Up</title>
</head>
<body>

<%@include file="navbar.jsp"%>
<div class="container-fluid" id="signup-page">
    <div class="container-fluid" id="signup-float">
        <div>
            <form class="form-horizontal" action="/SignUp" method="post" enctype="multipart/form-data">
                <fieldset>

                    <legend class="legend"><strong>Sign Up.</strong></legend>
                    <hr>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label" for="username">Username</label>
                            <div class="input col-md-12">
                                <input id="username" name="username" type="text" placeholder="Enter your username"
                                       class="form-control input-md" required="">

                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label" for="firstname">First Name</label>
                            <div class="input col-md-12">
                                <input id="firstname" name="firstname" type="text" placeholder="Enter your first name"
                                       class="form-control input-md" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="control-label" for="lastname">Last Name</label>
                            <div class="input col-md-12">
                                <input id="lastname" name="lastname" type="text" placeholder="Enter your last name"
                                       class="form-control input-md" required="">
                            </div>
                        </div>


                        <div class="form-group col-md-6">
                            <label class="control-label" for="middlename">Middle Name(s)</label>
                            <div class="input col-md-12">
                                <input id="middlename" name="middlename" type="text"
                                       placeholder="Enter your middle name(s)"
                                       class="form-control input-md">
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label class="control-label" for="email">Email</label>
                            <div class="input col-md-12">
                                <input id="email" name="email" type="text" placeholder="Enter your email address"
                                       class="form-control input-md" required>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="control-label" for="birthday">Date of Birth</label>
                            <div class="input col-md-12">
                                <input type="date" id="birthday" placeholder="Your birth date"
                                       class="form-control input-md"
                                       name="birthday" required="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="country">Select Your Country: </label>
                            <br/>
                            <div class="input col-md-12">
                                <select id="country" name="country" class="form-control" required>


                                    <% String[] countriesArray = countriesList.split(",");
                                        for (int i = 0; i < countriesArray.length; i += 2) { %>
                                    <option value="<%=countriesArray[i]%>"><%=countriesArray[i + 1]%>
                                    </option>
                                    <% } %>

                                </select>
                            </div>
                        </div>
                    </div>
                    <br>


                </fieldset>

                <div class="form-group">
                    <fieldset>
                        <legend class="legend"><strong>Select avatar.</strong></legend>
                        <hr>

                        <div class="form-group">
                            <% for (int i = 1; i < 6; i++) { %>
                            <input type="radio" id="avatar<%=i%>" value="avatar<%=i%>" name="system_avatars">
                            <label for="avatar<%=i%>"> <img src="/avatars/avatar<%=i%>.png" width="50px"></label>
                            <% } %>
                        </div>

                        <div class="input col-md-8">
                            <input type="file" class="btn btn-warning" name="file">
                        </div>

                    </fieldset>
                </div>
                <div class="input form-group col-md-12">
                    <label class="control-label" for="description">Description</label>
                    <div class="input col-md-12">
                        <textarea id="description" name="description" placeholder="Enter a description"
                                  class="form-control input-md" required=""></textarea>

                    </div>
                </div>
                <br>

                <div class="form-group">
                    <fieldset>
                        <legend class="legend"><strong>Password.</strong></legend>
                        <hr>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="pass1">Password</label>
                                <div class="input col-md-12">
                                    <input type="password" class="form-control" id="pass1"
                                           placeholder="Enter your new Password"
                                           name="password">
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="pass2">Re-enter Password</label>
                                <div class="input col-md-12">
                                    <input type="password" class="form-control" id="pass2"
                                           placeholder="Re-enter your password"
                                           name="password_confirm">
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div>
                        <p id="password_reminder"></p>
                    </div>
                </div>
                <div class="input col-md-8">
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>


            </form>
        </div>
    </div>
</div>
<%!
    String countriesList = "NZ,New Zealand,AU,Australia,CN,China,AF,Afghanistan,AL,Albania,DZ,Algeria,AS,American Samoa,AD,Andorra,AO,Angola,AI,Anguilla,AQ,Antarctica,AG,Antigua and Barbuda,AR,Argentina,AM,Armenia,AW,Aruba,AT,Austria,AZ,Azerbaijan,BS,Bahamas,BH,Bahrain,BD,Bangladesh,BB,Barbados,BY,Belarus,BE,Belgium,BZ,Belize,BJ,Benin,BM,Bermuda,BT,Bhutan,BO,Bolivia,BQ,Bonaire,BA,Bosnia and Herzegovina,BW,Botswana,BV,Bouvet Island,BR,Brazil,IO,British Indian Ocean Territory,BN,Brunei Darussalam,BG,Bulgaria,BF,Burkina Faso,BI,Burundi,KH,Cambodia,CM,Cameroon,CA,Canada,CV,Cape Verde,KY,Cayman Islands,CF,Central African Republic,TD,Chad,CL,Chile,CX,Christmas Island,CC,Cocos (Keeling) Islands,CO,Colombia,KM,Comoros,CG,Congo,CD,Democratic Republic of the Congo,CK,Cook Islands,CR,Costa Rica,HR,Croatia,CU,Cuba,CW,Curacao,CY,Cyprus,CZ,Czech Republic,CI,Cote d'Ivoire,DK,Denmark,DJ,Djibouti,DM,Dominica,DO,Dominican Republic,EC,Ecuador,EG,Egypt,SV,El Salvador,GQ,Equatorial Guinea,ER,Eritrea,EE,Estonia,ET,Ethiopia,FK,Falkland Islands (Malvinas),FO,Faroe Islands,FJ,Fiji,FI,Finland,FR,France,GF,French Guiana,PF,French Polynesia,TF,French Southern Territories,GA,Gabon,GM,Gambia,GE,Georgia,DE,Germany,GH,Ghana,GI,Gibraltar,GR,Greece,GL,Greenland,GD,Grenada,GP,Guadeloupe,GU,Guam,GT,Guatemala,GG,Guernsey,GN,Guinea,GW,Guinea-Bissau,GY,Guyana,HT,Haiti,HM,Heard Island and McDonald Mcdonald Islands,VA,Holy See (Vatican City State),HN,Honduras,HK,Hong Kong,HU,Hungary,IS,Iceland,IN,India,ID,Indonesia,IR,Iran, Islamic Republic of,IQ,Iraq,IE,Ireland,IM,Isle of Man,IL,Israel,IT,Italy,JM,Jamaica,JP,Japan,JE,Jersey,JO,Jordan,KZ,Kazakhstan,KE,Kenya,KI,Kiribati,KP,Korea, Democratic People's Republic of,KR,Korea, Republic of,KW,Kuwait,KG,Kyrgyzstan,LA,Lao People's Democratic Republic,LV,Latvia,LB,Lebanon,LS,Lesotho,LR,Liberia,LY,Libya,LI,Liechtenstein,LT,Lithuania,LU,Luxembourg,MO,Macao,MK,Macedonia, the Former Yugoslav Republic of,MG,Madagascar,MW,Malawi,MY,Malaysia,MV,Maldives,ML,Mali,MT,Malta,MH,Marshall Islands,MQ,Martinique,MR,Mauritania,MU,Mauritius,YT,Mayotte,MX,Mexico,FM,Micronesia, Federated States of,MD,Moldova, Republic of,MC,Monaco,MN,Mongolia,ME,Montenegro,MS,Montserrat,MA,Morocco,MZ,Mozambique,MM,Myanmar,NA,Namibia,NR,Nauru,NP,Nepal,NL,Netherlands,NC,New Caledonia,NI,Nicaragua,NE,Niger,NG,Nigeria,NU,Niue,NF,Norfolk Island,MP,Northern Mariana Islands,NO,Norway,OM,Oman,PK,Pakistan,PW,Palau,PS,Palestine, State of,PA,Panama,PG,Papua New Guinea,PY,Paraguay,PE,Peru,PH,Philippines,PN,Pitcairn,PL,Poland,PT,Portugal,PR,Puerto Rico,QA,Qatar,RO,Romania,RU,Russian Federation,RW,Rwanda,RE,Reunion,BL,Saint Barthelemy,SH,Saint Helena,KN,Saint Kitts and Nevis,LC,Saint Lucia,MF,Saint Martin (French part),PM,Saint Pierre and Miquelon,VC,Saint Vincent and the Grenadines,WS,Samoa,SM,San Marino,ST,Sao Tome and Principe,SA,Saudi Arabia,SN,Senegal,RS,Serbia,SC,Seychelles,SL,Sierra Leone,SG,Singapore,SX,Sint Maarten (Dutch part),SK,Slovakia,SI,Slovenia,SB,Solomon Islands,SO,Somalia,ZA,South Africa,GS,South Georgia and the South Sandwich Islands,SS,South Sudan,ES,Spain,LK,Sri Lanka,SD,Sudan,SR,Suriname,SJ,Svalbard and Jan Mayen,SZ,Swaziland,SE,Sweden,CH,Switzerland,SY,Syrian Arab Republic,TW,Taiwan, Province of China,TJ,Tajikistan,TZ,United Republic of Tanzania,TH,Thailand,TL,Timor-Leste,TG,Togo,TK,Tokelau,TO,Tonga,TT,Trinidad and Tobago,TN,Tunisia,TR,Turkey,TM,Turkmenistan,TC,Turks and Caicos Islands,TV,Tuvalu,UG,Uganda,UA,Ukraine,AE,United Arab Emirates,GB,United Kingdom,US,United States,UM,United States Minor Outlying Islands,UY,Uruguay,UZ,Uzbekistan,VU,Vanuatu,VE,Venezuela,VN,Viet Nam,VG,British Virgin Islands,VI,US Virgin Islands,WF,Wallis and Futuna,EH,Western Sahara,YE,Yemen,ZM,Zambia,ZW,Zimbabwe";
    ;%>

</body>
</html>
