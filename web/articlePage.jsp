<%--
  Created by IntelliJ IDEA.
  User: jesseskralskis
  Date: 5/21/18
  Time: 4:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main_styling.css" type="text/css">
    <%--<link rel="stylesheet" href="style/js/main-script.js">--%>
    <title>Browse</title>
</head>
<body>
<%@include file="navbar.jsp"%>
<div class="container-fluid" id="article-page">
    <div class="container-fluid" id="article-float">
        <div class="row">
            <div class="card col-md-6 col-xs-12">
                <%--<div class="img-container">--%>
                <%--<img class="card-img-top rounded-0" src="">--%>
                <%--</div>--%>
                <div class="card-body">
                    <h4 class="card-title">title goes here</h4>
                    <hr>
                    <p class="card-text">
                        content goes here
                        ipsum, a tempus risus. Etiam mattis eros varius ligula porta, non placerat diam
                        consequat.
                        Duis
                        volutpat tellus purus, eu sollicitudin justo rhoncus sit amet
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi et
                        posuere
                        ipsum, a tempus risus. Etiam mattis eros varius ligula porta, non placerat diam
                        consequat.
                        Duis
                        volutpat tellus purus, eu sollicitudin justo rhoncus sit amet.</p>
                    <p class="user-info">author, time, date goes here</p>
                    <a class="btn btn-warning">Read More</a>
                </div>
            </div>
            <div class="card col-md-6 col-xs-12">
                <%--<div class="img-container">--%>
                <%--<img class="card-img-top rounded-0" src="">--%>
                <%--</div>--%>
                <div class="card-body">
                    <h4 class="card-title">another article</h4>
                    <hr>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi et
                        posuere
                        ipsum, a tempus risus. Etiam mattis eros varius ligula porta, non placerat diam
                        consequat.
                        Duis
                        volutpat tellus purus, eu sollicitudin justo rhoncus sit amet.</p>
                    <p class="user-info">author, time, date goes here</p>
                    <a class="btn btn-warning">Read More</a>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
