<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@include file="style/style_head.jsp" %>
    <link rel="stylesheet" href="style/css/main_styling.css" type="text/css">
    <link rel="stylesheet" href="style/js/main-script.js">
    <title>Welcome</title>
</head>

<%@include file="navbar.jsp"%>
<body>
<div class="container-fluid" id="welcome-page">
    <div class="container-fluid" id="welcome-float">
        <h1 id="title" style="text-align: center"><strong>Technology.</strong></h1>
        <p>A blog from the future, for now.</p>
        <a href="signup.jsp" class="btn btn-warning">Sign Up</a>
        <a href="login.jsp" class="btn btn-warning">Login</a>
        <a href="articlePage.jsp" class="btn btn-primary">Enter</a>
        <div class="container" id="other-links">
            <a class="text-link"><strong>Contact.</strong></a>
            <a class="text-link"><strong>About.</strong></a>
        </div>


    </div>
</div>

</body>
</html>
