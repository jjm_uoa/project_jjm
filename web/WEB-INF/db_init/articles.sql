drop TABLE IF EXISTS finalProjectArticle;



CREATE TABLE finalProjectArticle (
  article_id   INT         NOT NULL,
  author_fname VARCHAR(60) NOT NULL,
  user_id int,
  author_lname VARCHAR(60),
  title        VARCHAR(60),
  content      VARCHAR(2000),
  created_time TIMESTAMP,
  PRIMARY KEY (article_id),
  FOREIGN KEY (user_id) REFERENCES account(id)



);







