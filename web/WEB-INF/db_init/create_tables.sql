drop TABLE IF EXISTS password_specs;
drop table if EXISTS account;
drop table IF EXISTS country;
drop table if EXISTS article;




CREATE TABLE country (
  code VARCHAR(2) NOT NULL,
  name VARCHAR(60) NOT NULL,
  PRIMARY KEY (code),
  UNIQUE INDEX country_code (code ASC),
  UNIQUE INDEX name (name ASC))
  ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE account (
  id int(11) NOT NULL AUTO_INCREMENT,
  login varchar(60) NOT NULL,
  firstname varchar(60) NOT NULL,
  lastname varchar(60) NOT NULL,
  middlename varchar(100) DEFAULT NULL,
  password_hash varchar(100) NOT NULL,
  email varchar(80) NOT NULL,
  birthday date NOT NULL,
  country varchar(2) NOT NULL,
  avatar_path varchar(255) NOT NULL,
  about varchar(500) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY username_UNIQUE (login),
  UNIQUE KEY email_UNIQUE (email),
  KEY code (country),
  CONSTRAINT country FOREIGN KEY (country) REFERENCES country (code) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE password_specs (
  id int(11) NOT NULL,
  salt BLOB NOT NULL ,
  iterations int(11) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT password_specs FOREIGN KEY (id) REFERENCES account (id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE article (
  article_id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  title varchar(255) NOT NULL,
  content longtext NOT NULL,
  created_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (article_id),
  KEY (user_id),
  CONSTRAINT author FOREIGN KEY (user_id) REFERENCES account (id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



drop table if EXISTS finalProjectArticle;
CREATE TABLE finalProjectArticle (
  article_id int  AUTO_INCREMENT not null,
  author_fname VARCHAR(60) NOT NULL,
  user_id int not null,
  author_lname VARCHAR(60),
  title        VARCHAR(60),
  content      VARCHAR(2000),
  created_time TIMESTAMP,
  PRIMARY KEY (article_id),
  FOREIGN KEY (user_id) REFERENCES account(id)





);

INSERT into finalProjectArticle (author_fname, author_lname,user_id, title, content, created_time) VALUES
  ('bart', 'simpson',3, 'a title', 'dsalkfgjhsdflgkjhglkj', now());






