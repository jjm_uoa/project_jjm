insert into account (login, firstname, lastname, middlename, password_hash, email, birthday, country, avatar_path, about)
VALUES
  ('user1', 'firstname1', 'lastname1', 'middlename1', 'hash', 'firstname1lastname1@gmail.com', '1970-01-01', 'NZ',
   'images/avatar.jpg', 'about');

insert into account (login, firstname, lastname, middlename, password_hash, email, birthday, country, avatar_path, about)
VALUES
  ('user2', 'firstname2', 'lastname2', 'middlename2', 'hash', 'firstname1lastname2@gmail.com', '1970-01-01', 'NZ',
   'images/avatar.jpg', 'about');

insert into account (login, firstname, lastname, middlename, password_hash, email, birthday, country, avatar_path, about)
VALUES
  ('user3', 'firstname3', 'lastname3', 'middlename3', 'hash', 'firstname1lastname3@gmail.com', '1970-01-01', 'NZ',
   'images/avatar.jpg', 'about');

insert into article (user_id, title, content) values
  (3, 'An Australian court has found a Catholic archbishop guilty of concealing child sexual abuse in the 1970s', 'Philip Wilson, the archbishop of Adelaide, becomes the most senior Catholic in the world to be charged and convicted of the offence.

He was found to have covered up the abuse of altar boys by a paedophile priest colleague in New South Wales.

During his trial he denied being told about the abuse by some of the victims.

In a statement issued by the church on Wednesday, Wilson said he was "obviously disappointed" with the verdict and would consider his legal options.'),
  (4, 'Wanted to protect the church', 'Last month, Wilson told the Newcastle Local Court he had no knowledge of priest James Fletcher''s actions, which took place when he was an assistant priest in Maitland, 130km (80 miles) north of Sydney.

Fletcher was later convicted of nine child sexual abuse charges in 2004, and died in jail in 2006.

One of his victims, former altar boy Peter Creigh, told the court he had described the abuse to Wilson in detail in 1976, five years after it took place.'),
  (4, 'Fuel price rise threatens consumer spending', 'At the pumps, the average price of petrol has risen to 127.22p a litre and diesel to 129.96p a litre, following a rapid rise in the oil price.

Recent data suggests the year-long squeeze on incomes has begun to ease, with wages growing faster than prices.

But rising fuel prices threaten to prevent inflation slowing more quickly.

"Things have started to look better for the UK consumer recently, with inflationary pressures easing and real wage growth finally started picking up," said George Salmon, equity analyst at Hargreaves Lansdown.

However, he said, that drivers had noticed the impact of higher fuel prices at the pumps.

"Filling up the tank is a pretty essential expense for most of us, so the average consumer could find there is a few pounds less in the jar at the end of each mont'),

  (5, 'Temporary disruption', 'However, Ruth Gregory, chief UK economist at Capital Economics said she expected the impact of higher fuel prices on the UK consumer to remain limited.

"We''re expecting the oil price to drift lower by the end of next year. The recent rise mostly reflects geopolitical tension and the potential risk of supply disruption, factors we think should prove temporary."

In the meantime, the overall trend for rising wages would continue she said.

"We''ve seen clear signs of a revival of pay growth in recent figures and we are expecting a further tick up to around 3% towards the end of this year."');

# insert into password_specs (id, salt, iterations) VALUES (2, '2', '3');