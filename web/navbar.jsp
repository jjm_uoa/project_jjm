<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 22/05/18
  Time: 1:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top" style="background-color: black">
    <a onclick="openNav()" class="navbar-brand" href="#">Logo</a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
        </li>
    </ul>
</nav>
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="index.jsp">Welcome</a>
    <a href="articlePage.jsp">Articles</a>
    <a href="signup.jsp">SignUp</a>
    <a href="login.jsp">Login</a>
    <a href="article.jsp">Add Article</a>
</div>

<script>
function openNav() {
document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
document.getElementById("mySidenav").style.width = "0";
}
</script>
